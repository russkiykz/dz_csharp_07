﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ_07_25._12._2020
{
    public class RangeOfArray
    {
        private int[] data;
        public int Superscript { get; set; }
        public int Subscript { get; set; }

        public RangeOfArray(int size)
        {
            data = new int[size];
        }

        public int this[int index]
        {
            get
            {
                if (index >= Subscript && index <= Superscript)
                {
                    return data[index - Subscript];
                }
                return -1;
            }
            set
            {
                if (index >= Subscript && index <= Superscript)
                {
                    data[index - Subscript]=value;
                }
            }
        }
    }
}
