﻿using System;

namespace DZ_07_25._12._2020
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            Console.Write("Введите нижнее значение диапазона: ");
            int indexMin;
            if (int.TryParse(Console.ReadLine(), out indexMin)) { };
            Console.Write("Введите верхнее значение диапазона: ");
            int indexMax;
            if (int.TryParse(Console.ReadLine(), out indexMax)) { };

            var array = new RangeOfArray(indexMax-indexMin+1)
            {
                Subscript = indexMin,
                Superscript = indexMax
            };

            for (int i = indexMin; i <= indexMax; i++)
            {
                array[i] = random.Next(0,30);
            }
            for (int i = indexMin; i <= indexMax; i++)
            {
                Console.WriteLine($"Значение с индексом {i} равно {array[i]}");
            }

        }
    }
}
